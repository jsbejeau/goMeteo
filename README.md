# goMeteo

goMeteo is a wrapper on openweathermap.org API.

The program start a WebServer and create endpoint to get useful informations from openweathermap.org.
The helper will connect at the begining of the execution to openweathermap.org API from environment variable : METEO_API_KEY

A first endpoint is available by default and give the current temperature for each city requested.
For example : 

```bash
    curl -X GET http://localhost:12345/temperature?city=Montreal&city=Moscow&city=Blabla
```

This request will return a JSON object similar to this : 

```json
{
  "data": {
    "Montreal": 27,
    "Moscow": 25.84
  },
  "errors": [
    "Can not find this city :Blabla"
  ]
}
```


