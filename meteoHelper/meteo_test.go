package meteoHelper

import (
	"bufio"
	"compress/gzip"
	"log"
	"net/http"
	"net/url"
	"os"
	"testing"
)

func getMeteoHelper() *MeteoHelper {
	return &MeteoHelper{}
}

func tearDown() {
	os.Remove("file.txt")
	os.Remove("wrongGZ.gz")
	os.Remove("valid.gz")
	os.Remove("file")
}

func TestGetCitiesFromRequest(t *testing.T) {

	m := getMeteoHelper()

	tt := []struct {
		name     string
		req      *http.Request
		expected map[string]struct{}
	}{
		{"nil request", nil, nil},
		{"request no params", &http.Request{}, nil},
		{"valid param", &http.Request{URL: &url.URL{RawQuery: "city=Montreal"}}, map[string]struct{}{"Montreal": struct{}{}}},
	}

	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			d := m.getCitiesFromRequest(tc.req)

			if tc.expected == nil && d != nil {
				t.Fatalf("getCitiesFromRequest expects %q, got %q", tc.expected, d)
			}

			if tc.expected != nil {
				if d == nil {
					t.Fatalf("getCitiesFromRequest expects %q, got %q", tc.expected, d)
				}

				if len(d) != len(tc.expected) {
					t.Fatalf("getCitiesFromRequest expects %q, got %q", tc.expected, d)
				}

				for _, city := range d {
					_, ok := tc.expected[city]
					if !ok {
						t.Fatalf("getCitiesFromRequest expects %q, got %q", tc.expected, d)
					}
				}
			}
		})
	}
}

func createGZ(filename string, data []byte) {

	fi, err := os.OpenFile(filename, os.O_WRONLY|os.O_APPEND|os.O_CREATE, 0660)
	if err != nil {
		log.Printf("Error in Create\n")
		panic(err)
	}
	gf := gzip.NewWriter(fi)
	fw := bufio.NewWriter(gf)

	fw.Write(data)
	fw.Flush()
	gf.Close()
	fi.Close()

	return
}

func TestLoadCityFile(t *testing.T) {
	tearDown()
	m := getMeteoHelper()
	defer tearDown()

	tt := []struct {
		name     string
		filename string
		expected string
		data     map[string]int64
		init     func()
	}{
		{"no file", "", "open : no such file or directory", nil, nil},
		{"no gzip file", "file.txt", "EOF", nil, func() { os.OpenFile("file.txt", os.O_RDONLY|os.O_CREATE, 0666) }},
		{"wrong json file", "wrongGZ.gz", "unexpected end of JSON input", nil, func() {
			createGZ("wrongGZ.gz", []byte("{"))
		}},
		{"valid", "valid.gz", "", map[string]int64{"MyCity": 123456}, func() {
			createGZ("valid.gz", []byte("[{\"id\":123456, \"name\":\"MyCity\"}]"))
		}},
	}

	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			if tc.init != nil {
				tc.init()
			}

			err := m.loadCityFile(tc.filename)

			if err == nil && tc.expected != "" ||
				(err != nil && (err.Error() != tc.expected)) {
				t.Fatalf("loadCityFile expects %s, got %s", tc.expected, err.Error())
			}

			if len(tc.data) != len(m.cityNameToID) {
				t.Fatalf("loadCityFile data shoud be %q, got %q", tc.data, m.cityNameToID)
			}

			for key, value := range tc.data {
				if m.cityNameToID[key] != value {
					t.Fatalf("loadCityFile data shoud be %q, got %q", tc.data, m.cityNameToID)
				}
			}

		})
	}

}

func TestDownloadCityListFromURL(t *testing.T) {

	tearDown()
	m := getMeteoHelper()
	defer tearDown()

	defaultValue := cityListURL
	defer func() { cityListURL = defaultValue }()

	tt := []struct {
		name     string
		filename string
		url      string
		expected string
		init     func()
	}{
		{"empty filename", "", "", "filenName can't be empty", nil},
		{"no url", "test", "", "Get : unsupported protocol scheme \"\"", nil},
		{"unable to create file", "./fakeFolder/test", defaultValue, "open ./fakeFolder/test: no such file or directory", nil},
	}

	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			if tc.init != nil {
				tc.init()
			}

			cityListURL = tc.url
			err := m.downloadCityListFromURL(tc.filename)
			if err == nil && tc.expected != "" ||
				(err != nil && (err.Error() != tc.expected)) {
				t.Fatalf("downloadCityListFromURL expects %s, got %q", tc.expected, err)
			}

		})
	}
}
