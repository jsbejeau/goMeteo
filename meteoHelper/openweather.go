package meteoHelper

type openWeatherSeveralCities struct {
	List []cityData `json:"list"`
}

type cityData struct {
	Main MainData `json:"main"`
	Name string   `json:"name"`
}

type MainData struct {
	Temp float64 `json:"temp"`
}
