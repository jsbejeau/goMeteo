package meteoHelper

import (
	"compress/gzip"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"
	"sync"
)

var cityListURL = "http://bulk.openweathermap.org/sample/city.list.json.gz"

// MeteoHelper : Helper to interact with http://openweathermap.org/
type MeteoHelper struct {
	// Public
	Status error

	// Private
	sync.RWMutex
	initialize sync.Once

	cityNameToID map[string]int64
	apiKey       string
}

// Init : initialize MeteoHelper struct from outside package
func (m *MeteoHelper) Init() error {
	m.initialize.Do(m.init)
	return m.Status
}

func (m *MeteoHelper) init() {
	err := m.loadCityDatabase()
	if err != nil {
		m.Status = fmt.Errorf("Init failed with this error : %q", err)
		return
	}

	m.apiKey = os.Getenv("METEO_API_KEY")
	if m.apiKey == "" {
		m.Status = fmt.Errorf("Init failed with this error : %s", "Can't not connect to openweathermap API, METEO_API_KEY is not defined")
		return
	}
}

// GetTemperatureByCity : return JSON array of temperature by city in request URL
func (m *MeteoHelper) GetTemperatureByCity(w http.ResponseWriter, req *http.Request) {
	m.initialize.Do(m.init)

	if req.Method != "GET" {
		w.WriteHeader(http.StatusMethodNotAllowed)
		return
	}

	cities := m.getCitiesFromRequest(req)
	if cities == nil || len(cities) == 0 {
		return
	}

	m.callOpenWeatherAPI(cities, w)

}

func (m *MeteoHelper) getCitiesFromRequest(req *http.Request) []string {

	if req == nil {
		return nil
	}

	if req.URL == nil {
		return nil
	}

	queryParams := req.URL.Query()
	return queryParams["city"]
}

func (m *MeteoHelper) callOpenWeatherAPI(cities []string, w http.ResponseWriter) {

	URL := "http://api.openweathermap.org/data/2.5/group?"

	type result struct {
		Data  map[string]float64 `json:"data"`
		Error []string           `json:"errors,omitempty"`
	}

	datas := &result{}

	listIDs := ""
	m.Lock()
	for _, city := range cities {
		ID, ok := m.cityNameToID[city]
		if !ok {
			datas.Error = append(datas.Error, "Can not find this city :"+city)
			continue
		}
		listIDs += strconv.FormatInt(ID, 10) + ","
	}
	m.Unlock()

	if listIDs == "" {
		return
	}

	listIDs = listIDs[:len(listIDs)-1]

	URL += "id=" + listIDs
	URL += "&units=metric"
	URL += "&appid=" + m.apiKey

	resp, err := http.Get(URL)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	defer resp.Body.Close()
	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	apiDatas := &openWeatherSeveralCities{}
	err = json.Unmarshal(b, &apiDatas)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if len(apiDatas.List) == 0 {
		http.Error(w, string(b), http.StatusBadRequest)
		return
	}

	datas.Data = make(map[string]float64)
	for _, cityData := range apiDatas.List {
		datas.Data[cityData.Name] = cityData.Main.Temp
	}

	bFin, err := json.Marshal(&datas)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	writeResult(w, bFin)
}

func writeResult(w http.ResponseWriter, data []byte) {

	s := fmt.Sprintf("%s", data)
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Content-Type", "application/json")
	fmt.Fprintf(w, s)
	f := w.(http.Flusher)
	f.Flush()
}

// loadCityDatabase : use local file first or try to download file if not exist
// in case of error on local file, download file from url and retry
func (m *MeteoHelper) loadCityDatabase() error {

	fileName := "./city.list.json.gz"
	local := true

	// Check if list of City are already available localy
	_, err := os.Stat(fileName)
	if os.IsNotExist(err) {
		log.Println("Load database from URL")
		local = false
		err := m.downloadCityListFromURL(fileName)
		if err != nil {
			return err
		}
	} else {
		log.Println("Load database from local file")
	}

	err = m.loadCityFile(fileName)
	if err == nil {
		return nil
	}

	if !local {
		return err
	}

	log.Println("error loading database from local file :", err)

	// If local, possible corrupted file, try to download it
	err = os.Remove(fileName)
	if err != nil {
		return err
	}

	return m.loadCityDatabase()
}

func (m *MeteoHelper) downloadCityListFromURL(fileName string) error {

	if fileName == "" {
		return errors.New("filenName can't be empty")
	}

	resp, err := http.Get(cityListURL)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	output, err := os.Create(fileName)
	if err != nil {
		return err
	}
	defer output.Close()
	_, err = io.Copy(output, resp.Body)
	if err != nil {
		return err
	}
	return nil
}

func (m *MeteoHelper) loadCityFile(fileName string) error {

	file, err := os.Open(fileName)
	if err != nil {
		return err
	}

	gz, err := gzip.NewReader(file)
	if err != nil {
		return err
	}

	defer file.Close()
	defer gz.Close()

	b, err := ioutil.ReadAll(gz)
	if err != nil {
		return err
	}

	type city struct {
		ID   int64  `json:"id"`
		Name string `json:"name"`
	}

	cities := make([]city, 0, 0)

	err = json.Unmarshal(b, &cities)
	if err != nil {
		return err
	}

	m.Lock()
	if m.cityNameToID == nil {
		m.cityNameToID = make(map[string]int64)
	}

	for _, city := range cities {
		m.cityNameToID[city.Name] = city.ID
	}
	m.Unlock()

	return nil
}
