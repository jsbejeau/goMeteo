package main

import (
	"log"
	"net/http"

	"gitlab.com/jsbejeau/goMeteo/meteoHelper"
)

func main() {
	log.SetFlags(log.Lshortfile)
	meteo := meteoHelper.MeteoHelper{}
	err := meteo.Init()
	if err != nil {
		log.Fatal(err)
	}

	log.Println("Starting server")

	mux := http.NewServeMux()
	mux.HandleFunc("/temperature", meteo.GetTemperatureByCity)

	log.Fatal(http.ListenAndServe(":12345", mux))
}
